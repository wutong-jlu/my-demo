package com.jlu.mydemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.helper.widget.Layer;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class ListActivity extends AppCompatActivity {

    RadioGroup rg1;

    CheckBox cb1;

    Spinner sp1;

    ListView lv1;
    ListView lv2;

    Button btn1;
    Button btn2;
    Button btn3;

    EditText et1;

    TextView tv1;

    SharedPreferences sp;

    String[] str_arr = {"Kobe", "Jordan", "James"};
    String[] str_age = {"42", "69", "38"};

    String file_name = "tttt.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        rg1 = findViewById(R.id.rg1);
        rg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                RadioButton rb = findViewById(checkedId);
                Toast.makeText(ListActivity.this, rb.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

        cb1 = findViewById(R.id.cb1);
        cb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                String txt = isChecked ? "U checked RED!" : "U unchecked RED!";
                Toast.makeText(ListActivity.this, txt, Toast.LENGTH_SHORT).show();
            }
        });

        sp1 = findViewById(R.id.sp1);
        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                String[] my_array = getResources().getStringArray(R.array.my_arr);
                Toast.makeText(ListActivity.this, my_array[position], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        lv1 = findViewById(R.id.lv1);
        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String[] my_array = getResources().getStringArray(R.array.my_arr);
                Toast.makeText(ListActivity.this, my_array[position], Toast.LENGTH_SHORT).show();
            }
        });

        lv2 = findViewById(R.id.lv2);
//        ArrayAdapter<String> arrad = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, str_arr);
//        lv2.setAdapter(arrad);
        lv2.setAdapter(new MyAdapter(getApplicationContext()));

        sp = getSharedPreferences("config", MODE_PRIVATE);
        int count = sp.getInt("count", 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("count", count + 1);
        editor.commit();

        btn1 = findViewById(R.id.btn1);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ListActivity.this,
                        String.valueOf(sp.getInt("count", 0)),
                        Toast.LENGTH_SHORT).show();
            }
        });

        et1 = findViewById(R.id.et1);

        tv1 = findViewById(R.id.tv1);

        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    FileOutputStream fos = openFileOutput(file_name, MODE_PRIVATE);
                    PrintStream ps = new PrintStream(fos);
                    ps.print(et1.getText().toString());
                    ps.close();
                    fos.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    FileInputStream fis = openFileInput(file_name);
                    byte[] buffer = new byte[1024];
                    int hasRead = -1;
                    StringBuilder sb =  new StringBuilder("");
                    while((hasRead = fis.read(buffer)) > 0) {
                        sb.append(new String(buffer, 0, hasRead));
                    }
                    tv1.setText(sb.toString());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    class MyAdapter extends BaseAdapter {

        Context context;

        public MyAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return str_arr.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if(position % 2 == 0) {
                view = LayoutInflater.from(context).inflate(R.layout.item1, null);
            } else {
                view = LayoutInflater.from(context).inflate(R.layout.item2, null);
            }
            TextView tvv1 = view.findViewById(R.id.tvv1);
            TextView tvv2 = view.findViewById(R.id.tvv2);
            tvv1.setText(str_arr[position]);
            tvv2.setText(str_age[position]);
            return view;
        }
    }
}